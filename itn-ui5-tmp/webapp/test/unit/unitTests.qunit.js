/* global QUnit */
QUnit.config.autostart = false;

sap.ui.getCore().attachInit(function () {
	"use strict";

	sap.ui.require([
		"comistn./itn-ui5-tmp/test/unit/AllTests"
	], function () {
		QUnit.start();
	});
});
